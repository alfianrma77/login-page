/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <View style = {{
          backgroundColor: "#F7F7F7",
          paddingHorizontal: 20,
          paddingTop:140,
          flex:1
        }}>
          <View style = {styles.content}>
            <View style = {styles.title}>
              <Text style= {styles.titleText}>Digital Approval</Text>
            </View>
            <Image style = {styles.image} source={require('./assets/gambar.png')}/>
            <TextInput style = {styles.inputText} placeholder='Alamat Email' />
            <TextInput style = {styles.inputText} placeholder='Password'/>
            <Text style = {styles.resetPass}>Reset Password</Text>
            <TouchableOpacity style={styles.btnLogin}>
              <Text style={styles.login}>Login</Text>
            </TouchableOpacity>
          </View>          
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  content:{
    backgroundColor: "white",
    paddingHorizontal:16,
    paddingBottom:16
  },
  image:{
    alignSelf:"center",
    marginBottom:38
  },
  title: {
    backgroundColor: "#002558",
    marginTop: -25,
    borderRadius: 50,
    marginBottom:27,
    marginHorizontal:60
  },
  titleText: {
    fontWeight : "bold",
    fontSize: 20,
    color: "white",
    textAlign: "center",
    paddingVertical: 8,
    paddingHorizontal: 10
  },
  inputText: {
    borderWidth: 1,
    borderColor: "#002558",
    fontSize:17,
    marginBottom:24,
    paddingHorizontal:20
  },
  resetPass:{
    fontSize:20,
    fontStyle:"italic",
    fontWeight:"bold",
    alignSelf:'flex-end',
    color:"#287AE5",
    marginBottom:24
  },
  btnLogin:{
    alignItems:"center",
    paddingHorizontal:16,
    backgroundColor:"#287AE5",
    borderRadius:15
  },
  login:{
    fontSize:20,
    fontWeight:"bold",
    color:"white",
    paddingVertical:13,
  }
});

export default App;
